# Replace two colors of an icon with QPixmap and QPainter (PyQt5)
# Require a monochrome image on a transparent background

background, foreground = "red", "yellow"
pixmap = QtGui.QPixmap("icon.svg")
painter = QtGui.QPainter(pixmap)

painter.setCompositionMode(painter.CompositionMode_Xor)
painter.fillRect(pixmap.rect(), QtGui.QColor(background))

painter.setCompositionMode(painter.CompositionMode_Overlay)
painter.fillRect(pixmap.rect(), QtGui.QColor(foreground))

painter.end()
icon = QtGui.QIcon(pixmap)
