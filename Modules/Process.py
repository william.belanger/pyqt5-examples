#!/usr/bin/python3
import sys
from PyQt5 import QtWidgets, QtCore


class Slave(QtCore.QProcess):
    def __init__(self, parent=None):
        super().__init__()
        self.readyReadStandardOutput.connect(self.stdoutEvent)
        self.readyReadStandardError.connect(self.stderrEvent)

    def stdoutEvent(self):
        stdout = self.readAllStandardOutput()
        self.echo(stdout)

    def stderrEvent(self):
        stderr = self.readAllStandardError()
        self.echo(stderr)

    def echo(self, data):
        data = bytes(data).decode("utf8")
        if data:
            print(data, end="")


class Main(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.slave = Slave()
        self.slave.start("/home/user/my_process")


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    gui = Main()
    sys.exit(app.exec_())
