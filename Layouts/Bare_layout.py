#!/usr/bin/python3
import sys
from PyQt5 import QtCore, QtGui, QtWidgets


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.ui = QtWidgets.QWidget(self)
        self.setCentralWidget(self.ui)
        self.ui.button = QtWidgets.QPushButton("Close")
        self.ui.button.clicked.connect(self.close)
        self.ui.layout = QtWidgets.QVBoxLayout()
        self.ui.layout.addWidget(self.ui.button)
        self.ui.setLayout(self.ui.layout)
        self.show()


if __name__== '__main__':
    app = QtWidgets.QApplication(sys.argv)
    gui = Main(app)
    sys.exit(app.exec_())
