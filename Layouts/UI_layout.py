#!/usr/bin/python3
import os
import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))


class Main(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = uic.loadUi(f"{LOCAL_DIR}/gui_main.ui", self)
        self.show()


if __name__== '__main__':
    app = QtWidgets.QApplication(sys.argv)
    gui = Main()
    sys.exit(app.exec_())
